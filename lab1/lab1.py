import numpy as np
from PIL import Image
import time
import matplotlib.pyplot as plt
import math as m

# PATH = '../pictures/xmpl.bmp'
# PATH = '../pictures/xmpl2.bmp'
PATH = '../pictures/circle.png'
# PATH = '../pictures/landscape.png'
mode = 'RGB'


def stretch(img, multiple):
    """Функция растяжения (интерполяции) изображения"""
    if multiple > 1:
        collector = 0
        for i in range(img.shape[1] - 1, -1, -1):
            collector += multiple - 1
            for _ in range(int(collector)):
                img = np.insert(img, i, img[:, i], axis=1)
            collector %= 1
        for j in range(img.shape[0] - 1, -1, -1):
            collector += multiple - 1
            for _ in range(int(collector)):
                img = np.insert(img, j, img[j, :], axis=0)
            collector %= 1
        return img
    else:
        print('m <= 1!!!')
        return False


def compression(img, n):
    """Функция сжатия (децимации) изображения"""
    if n <= 1:
        print("n <= 1!!!")
        return False
    else:
        collector = 0
        i = 0
        while i < img.shape[0]:
            collector += n - 1
            if collector >= 1:
                img = np.delete(img, np.s_[i + 1:i + int(collector) + 1], 0)
            i += 1
            collector %= 1
        collector = 0
        i = 0
        while i < img.shape[1]:
            collector += n - 1
            if collector >= 1:
                img = np.delete(img, np.s_[i + 1:i + int(collector) + 1], 1)
            i += 1
            collector %= 1
        return img


def resample_mn(img, m, n):
    img = stretch(img, m)
    img = compression(img, n)
    return img


def resample(img, k):
    if k > 1:
        return stretch(img, k)
    elif k < 1:
        return compression(img, 1/k)
    else:
        return img

# Вспомогательная функция открытия изображения
def open_image(p):
    try:
        img = Image.open(p).convert(mode)  # Открытие файла с изображением
        return np.asarray(img)  # Получения массива из изображения
    except Exception:
        print('Unable to open image\n')
        return np.ndarray([])


def test_compress():
    """Тестирование сжатия изображения"""
    img = open_image(PATH)
    new_img = compression(img, 2.5)
    print('Shape of image:', *img.shape)
    print('Shape of compressed image:', *new_img.shape)
    Image.fromarray(img).show(title='image.png')
    Image.fromarray(new_img).show('lab1/results/compressed_image.png')


def test_stretch():
    """Тестирование растяжения изображения"""
    img = open_image(PATH)
    new_img = stretch(img, 1.25)
    print('Shape of image:', *img.shape)
    print('Shape of stretched image:', *new_img.shape)
    Image.fromarray(img).show(title='image.png')
    Image.fromarray(new_img).show('lab1/results/stretched_image.png')


def test_resemp_mn():
    """Тестирование передискретизации (методом двойного прохода) изображения"""
    img = open_image(PATH)
    new_img = resample_mn(img, 2, 5)
    print('Shape of image:', *img.shape)
    print('Shape of resampled image:', *new_img.shape)
    Image.fromarray(img).show(title='image.png')
    Image.fromarray(new_img).show('lab1/results/resampled_image_mn.png')


def test_resemp_k():
    """Тестирование передискретизации изображения"""
    img = open_image(PATH)
    new_img = resample(img, 0.89)
    print('Shape of image:', *img.shape)
    print('Shape of resampled image:', *new_img.shape)
    Image.fromarray(img).show(title='image.png')
    Image.fromarray(new_img).show('lab1/results/resampled_image.png')



if __name__ == '__main__':
    test_resemp_mn()
