from lab1.lab1 import stretch
import os, sys
import numpy as np
import math as m
import csv
from PIL import Image
import matplotlib.pyplot as plt


def haralic(img):
    size = 256

    haralic_matrix = np.zeros((size, size))

    for x in range(1, img.shape[0] - 1):
        for y in range(1, img.shape[1] - 1):
            pixel = img[x, y]

            up_left_pixel = img[x-1, y-1]
            down_left_pixel = img[x-1, y+1]
            up_right_pixel = img[x+1, y-1]
            down_right_pixel = img[x+1, y+1]

            haralic_matrix[pixel, up_left_pixel] += 1
            haralic_matrix[pixel, down_left_pixel] += 1
            haralic_matrix[pixel, up_right_pixel] += 1
            haralic_matrix[pixel, down_right_pixel] += 1

    return haralic_matrix


def Pj(i, img):
    Pj = 0
    for j in range(img.shape[1]):
        Pj += img[i, j]
    return Pj


def Pi(j, img):
    Pi = 0
    for i in range(img.shape[0]):
        Pi += img[i, j]
    return Pi


def get_corr(haralic_matrix):

    mu_i = 0
    mu_j = 0
    for i in range(256):
        mu_i += (i + 1) * Pj(i, haralic_matrix)

    for j in range(256):
        mu_j += (j + 1) * Pi(j, haralic_matrix)

    sigma_i = 0
    sigma_j = 0

    for i in range(256):
        sigma_j += (i + 1 - mu_j) ** 2 * Pj(i, haralic_matrix)

    for j in range(256):
        sigma_i += (j + 1 - mu_i) ** 2 * Pi(j, haralic_matrix)

    sigma_i = np.sqrt(sigma_i)
    sigma_j = np.sqrt(sigma_j)

    corr = 0
    for i in range(256):
        for j in range(256):
            corr += (i - mu_i) * (j - mu_j) * haralic_matrix[i, j]
            # if mu_i - mu_j != 0:
            #     print('Ohh', mu_j - mu_i)

    corr = corr / (sigma_i * sigma_j)

    return corr


def hist_im(image):
    hist = np.array([0]*256)
    for row in image:
        for pix in row:
            hist[int(pix) if int(pix) < 256 else 255] += 1
    return hist


def contrast(img, l=3.5):
    hist = hist_im(img)
    Mean = np.mean(img)
    Max = tuple(hist).index(max(hist))
    Min = tuple(hist).index(min(hist))
    positive_range = max(2, Max - Mean)
    negative_range = max(2, Mean - Min)

    positive_alpha = 2**(l-1)/np.log(positive_range)
    negative_alpha = 2**(l-1)/np.log(negative_range)

    result = img
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            f = img[i, j] - Mean
            if f >= 1:
                result[i, j] = Mean + m.ceil(positive_alpha * np.log(f))
            elif f <= -1:
                result[i, j] = Mean - m.ceil(negative_alpha * np.log(-f))
            else:
                result[i, j] = Mean

    return result


if __name__ == '__main__':
    image_f = 'book'
    image = Image.open(f'{image_f}.png').convert('L')
    image.save(f'{image_f}.png')
    image = np.asarray(image)
    har = haralic(image)
    har_ = np.array([[pix if pix < 256 else 255 for pix in row] for row in har])
    Image.fromarray(har).show(f'{image_f}_har.png')
    with open(f'{image_f}_features.csv', 'w', newline='', encoding='UTF-8') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter='\t', )
        spamwriter.writerow(('Feature', 'Value'))
        spamwriter.writerow(('Original', get_corr(har_)))
        plt.plot(range(0, 256), hist_im(image))
        plt.savefig(f'{image_f}_hist.png')
        plt.close()

        image = contrast(image, l=-2)
        Image.fromarray(image).save(f'{image_f}_contr.png')
        har = haralic(image)
        har_ = np.array([[pix if pix < 256 else 255 for pix in row] for row in har])
        Image.fromarray(har).show(f'{image_f}_har_contr.png')
        spamwriter.writerow(('Contrasted', get_corr(har_)))
        plt.plot(range(0, 256), hist_im(image))
        plt.savefig(f'{image_f}_hist_contr')
