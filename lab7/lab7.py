import os

from lab5.lab5 import *
import matplotlib.pyplot as plt


def get_distance(x, y):
    if len(x) == len(y):
        return sum((x[i]-y[i])**2 for i in range(len(x)))
    else:
        raise ValueError('x and y has different dimensions')


if __name__ == '__main__':
    i = 0
    feat_dir = '../lab5/features/scalar'
    base_map = dict()

    n = 5

    for letter in os.listdir(feat_dir):
        letter_path = os.path.join(feat_dir, letter)
        vector_base = []
        with open(letter_path, newline='') as csvfile:
            reader = csv.reader(csvfile, delimiter='\t')
            for row in reader:
                if row[0] in (
                        'Удельный вес',
                        'Нормированные координаты цетра тяжести',
                        'Нормированный осевой момент инерции'
                              ):
                    if row[1][-1] == ')':
                        vector_base = vector_base + list(float(x) for x in row[1][1:-1].split(','))
                    else:
                        vector_base.append(float(row[1]))
        base_map[letter] = vector_base

    directory = 'chars3'
    for i in range(28):
        file = f'{directory}/{i}.png'
        if os.path.isfile(file):
            image = np.array([[True if pix > 150 else False for pix in row]
                              for row in np.asarray(Image.open(file).convert('L'))])
            vector = list(calc_spec_weight(image) + calc_rel_cg(image) + amoi_norm(image))
            # vector.append(sum(calc_weight(image))/(image.shape[0]*image.shape[1]))
            pred_letters = dict()
            for letter in base_map:
                pred_letters[letter] = 1 - get_distance(base_map[letter], vector)/get_distance(base_map[letter], (0,)*8)
            pred_letters_l = sorted(pred_letters, key=pred_letters.get, reverse=True)

            print(f'{i}:', *((letter, pred_letters[letter]) for letter in pred_letters_l[:]))
