import math
import numpy as np
import csv
from PIL import Image
import matplotlib.pyplot as plt


def calc_weight(img: np.ndarray):
    weight = [0, 0, 0, 0]
    for row in img[:img.shape[0] // 2]:
        for pixel in row[:img.shape[1] // 2]:
            if pixel != 0:
                weight[0] += pixel

    for row in img[:img.shape[0] // 2]:
        for pixel in row[img.shape[1] // 2:]:
            if pixel != 0:
                weight[1] += pixel

    for row in img[img.shape[0] // 2:]:
        for pixel in row[:img.shape[1] // 2]:
            if pixel != 0:
                weight[2] += pixel

    for row in img[img.shape[0] // 2:]:
        for pixel in row[img.shape[1] // 2:]:
            if pixel != 0:
                weight[3] += pixel

    return tuple(weight)


def calc_spec_weight(img: np.ndarray):
    return tuple(x * 4 / img.size for x in calc_weight(img))


def calc_cg(img: np.ndarray):
    weight = calc_weight(img)
    x = 0
    y = 0
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            x += i * img[i, j]
            y += j * img[i, j]
    return x / sum(weight), y / sum(weight)


def calc_rel_cg(img: np.ndarray):
    x, y = calc_cg(img)
    return (x - 1) / (img.shape[0] - 1), (y - 1) / (img.shape[1] - 1)


def amoi(img: np.ndarray):
    x_cg, y_cg = calc_cg(img)
    horizontal, vertical = 0, 0
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            vertical += img[i, j] * ((j - y_cg) ** 2)
            horizontal += img[i, j] * ((i - x_cg) ** 2)

    return vertical, horizontal


def amoi_norm(img: np.ndarray):
    vertical, horizontal = amoi(img)
    return \
        vertical / img.shape[0]**2 / img.shape[1]**2, \
        horizontal / img.shape[0]**2 / img.shape[1]**2
#sum([x**2 for x in range(img.shape[1])]) / img.shape[0],\
# sum([x**2 for x in range(img.shape[0])]) / img.shape[1]

def profile(img: np.ndarray):
    x_prof = []
    y_prof = []
    for i in range(img.shape[0]):
        x_prof.append(sum(img[i, :]))
    for j in range(img.shape[1]):
        y_prof.append(sum(img[:, j]))
    return x_prof, y_prof


def write_features(sign='alpha'):
    image = np.array([[True if pix > 150 else False for pix in row]
                              for row in np.asarray(Image.open(f'chars/{sign}.png').convert('L'))])

    with open(f'features/scalar/{sign}.csv', 'w', newline='') as csvfile:
        spamwriter = csv.writer(csvfile, delimiter='\t')
        spamwriter.writerow(('Характеристика', 'Значение'))
        spamwriter.writerow(('Вес', calc_weight(image)))
        spamwriter.writerow(('Удельный вес', calc_spec_weight(image)))
        spamwriter.writerow(('Центр тяжести', calc_cg(image)))
        spamwriter.writerow(('Нормированные координаты цетра тяжести', calc_rel_cg(image)))
        spamwriter.writerow(('Осевой момент инерции', amoi(image)))
        spamwriter.writerow(('Нормированный осевой момент инерции', amoi_norm(image)))
        spamwriter.writerow(('Удельный вес (полный)', sum(calc_weight(image))/(image.shape[0]*image.shape[1])))

    x = profile(image)
    plt.plot(x[0], np.linspace(0, len(x[0]), len(x[0]))[::-1])
    plt.savefig(f'features/charts/{sign}_prof_x.png')
    plt.close()
    plt.plot(x[1])
    plt.savefig(f'features/charts/{sign}_prof_y.png')
    plt.close()


if __name__ == '__main__':
    signs = ['alpha', 'beta', 'gamma', 'delta', 'chi', 'epsilon', 'eta', 'iota', 'kappa', 'lambda', 'mu', 'nu', 'omega',
             'omicron', 'phi', 'pi', 'psi', 'rho', 'sigma', 'tau', 'theta', 'upsilon', 'xi', 'zeta']
    for x in signs:
        write_features(x)
