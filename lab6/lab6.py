import math
import numpy as np
import csv
from PIL import Image
import matplotlib.pyplot as plt


def profile(img: np.ndarray):
    x_prof = []
    y_prof = []
    for i in range(img.shape[0]):
        x_prof.append(sum(img[i, :]))
    for j in range(img.shape[1]):
        y_prof.append(sum(img[:, j]))
    return x_prof, y_prof


if __name__ == '__main__':
    n = 2
    image1 = np.asarray(Image.open(f'text{n}.bmp', mode='r'))
    image = np.array([[True if sum(pix) < 100 * 3 else False for pix in row] for row in image1])
    y, x = profile(image)
    signs_x = []
    signs_y = []

    for j in range(image.shape[1]):
        if x[j] == 0:
            signs_x.append(j)

    for i in range(image.shape[0]):
        if y[i] == 0:
            signs_y.append(i)

    k = 0

    for x in range(len(signs_x) - 2, 0, -1):
        if signs_x[x] - signs_x[x - 1] <= 1 and signs_x[x+1] - signs_x[x] <= 1 + k:
            del signs_x[x]
            k += 1
        else:
            k = 0
    del signs_x[-1]
    del signs_x[0]


    for y in range(len(signs_y) - 2, 0, -1):
        if signs_y[y] - signs_y[y - 1] <= 1 and signs_y[y+1] - signs_y[y] <= 1 + k:
            del signs_y[y]
            k += 1
        else:
            k = 0
    del signs_y[0]
    del signs_y[-1]


    for i_letter in range(0, len(signs_x)//2):
        Image.fromarray(
            image1[signs_y[0]:signs_y[1]+1,
                   signs_x[i_letter*2]:signs_x[i_letter*2+1]+1]).save(f'chars{n}/{i_letter}.png')

    # plt.plot(x[0], np.linspace(0, len(x[0]), len(x[0]))[::-1])
    # plt.show()
    # plt.close()
    # plt.plot(x[1])
    # plt.show()
    # plt.close()
