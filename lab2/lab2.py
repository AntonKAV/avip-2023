import math
import numpy as np
from PIL import Image
import time
import matplotlib.pyplot as plt

# PATH = 'pictures/landscape.png'
# PATH = '../pictures/xmpl.bmp'
# PATH = '../pictures/xmpl2.bmp'
# PATH = 'pictures/face_noise.jpg'
PATH = '../pictures/book.png'
mode = 'RGB'


# Вспомогательная функция открытия изображения
def open_image(p):
    try:
        img = Image.open(p).convert(mode)  # Открытие файла с изображением
        # img.show()
        return np.asarray(img)  # Получения массива из изображения
    except Exception:
        print('Unable to open image\n')
        return np.ndarray([])


# Функция получения полутонового изображения в виде ndarray
def get_semitone(img):
    if isinstance(img[0, 0], np.uint8):
        return img
    new_img = np.zeros([img.shape[0], img.shape[1]], dtype=np.uint8)
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            k = img[i, j][0] * 0.3 + img[i, j][1] * 0.59 + img[i, j][2] * 0.11
            new_img[i, j] = k
    return new_img


def christian(img: Image, k=0.5, d=7):
    """Функция получения бинарного изображения"""
    img = get_semitone(img)
    Mean = np.zeros([img.shape[0], img.shape[1]])  # Матрица средних апертуры
    Var = np.zeros([img.shape[0], img.shape[1]])  # Матрица СКО апертуры
    x_max = img.shape[0] - 1  # Максимальный индекс по оси х изображения
    y_max = img.shape[1] - 1  # Максимальный индекс по оси у ихображения
    M = np.amin(img)  # Минимальное значение яркости изображения

    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            if i - d >= 0:
                x_high = i - d
            else:
                x_high = 0
            if j - d >= 0:
                y_left = j - d
            else:
                y_left = 0
            if i + d <= x_max:
                x_low = i + d
            else:
                x_low = x_max
            if j + d <= y_max:
                y_right = j + d
            else:
                y_right = y_max
            Mean[i, j] = np.mean(img[x_high:x_low + 1, y_left:y_right + 1])
            Var[i, j] = np.std(img[x_high:x_low + 1, y_left:y_right + 1])

    R = np.amax(Var)

    new_img = np.zeros([img.shape[0], img.shape[1]], dtype=bool)
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            T = (1 - k) * Mean[i, j] + k * M + k * Var[i, j] * (Mean[i, j] - M) / R
            if T > img[i, j]:
                new_img[i, j] = False
            else:
                new_img[i, j] = True
    return new_img


def smooth(hist: np.ndarray(shape=[256], dtype=np.uint8)):
    new_hist = np.zeros_like(hist)
    for i in range(hist.shape[0]):
        new_hist[i] = ((hist[i - 2] if i > 1 else 0) +
                       (2 * hist[i - 1] if i > 0 else 0) +
                       3 * hist[i] +
                       (2 * hist[i + 1] if i < hist.shape[0] - 1 else 0) +
                       (hist[i + 2] if i < hist.shape[0] - 2 else 0)) / 9
    return new_hist


def smooth_matr(matrix: np.ndarray):
    new_matr = np.array(matrix)
    mask = np.array([[2 ** (-1 / 2), 1, 2 ** (-1 / 2)],
                     [1, 2, 1],
                     [2 ** (-1 / 2), 1, 2 ** (-1 / 2)]])
    for i in range(1, new_matr.shape[0] - 1):
        for j in range(1, new_matr.shape[1] - 1):
            new_matr[i, j] = np.sum(matrix[i - 1:i + 2, j - 1:j + 2] * mask) / np.sum(mask)
    return new_matr


def th_matrix_bimodal(img, diam=7, delta=4, valley_grade=0.8, eps: tuple = (0.05, 2)):
    new_img = np.array(img)
    matr_th = np.zeros([math.ceil((img.shape[0] * 2) / diam),
                        math.ceil((img.shape[1] * 2) / diam)], dtype=np.ndarray)

    for _ in range(math.ceil(((matr_th.shape[0] + 1) * diam / 2 - new_img.shape[0]))):
        new_img = np.insert(new_img, new_img.shape[0], 0, axis=0)
    for _ in range(math.ceil(((matr_th.shape[1] + 1) * diam / 2 - new_img.shape[1]))):
        new_img = np.insert(new_img, new_img.shape[1], 0, axis=1)

    for i in range(matr_th.shape[0]):
        for j in range(matr_th.shape[1]):
            hist = smooth(get_hist(new_img[math.ceil(i * diam / 2):math.ceil(i * diam / 2) + diam,
                                   math.ceil(j * diam / 2):math.ceil(j * diam / 2) + diam]))
            if sum(hist) == 0:
                matr_th[i, j] = 0
                continue

            v = round(sum([i * hist[i] for i in range(hist.shape[0])]) / sum(hist))  # v is calculated as mean
            n1 = sum(hist[:v])
            n2 = sum(hist[v:])
            if n1 == 0 or n2 == 0:
                matr_th[i, j] = 0
                continue

            m1 = sum([hist[i] * i for i in range(v)]) / n1
            m2 = sum([hist[i] * i for i in range(v, 256)]) / n2
            if np.mean((hist[round(m1)], hist[round(m2)])) == 0:
                matr_th[i, j] = 0
                continue

            sigma1 = (sum([hist[i] * ((i - m1) ** 2) for i in range(v)]))
            sigma2 = (sum([hist[i] * ((i - m2) ** 2) for i in range(v, 256)]))
            if sigma2 == 0 or sigma1 == 0:
                matr_th[i, j] = 0
                continue

            sigma1 /= n1
            sigma2 /= n2
            p1 = sigma1 * n1 / sum([math.exp(-(((i - m1) ** 2) / (2 * sigma1 ** 2))) for i in range(v)])
            p2 = sigma2 * n2 / sum([math.exp(-(((i - m2) ** 2) / (2 * sigma2 ** 2))) for i in range(v, 256)])
            if (abs(m2 - m1) <= delta) or \
                    not (eps[0] < sigma1 / sigma2 < eps[1]) or \
                    (min(hist[round(m1):round(m2)]) / np.mean((hist[round(m1)], hist[round(m2)]))) >= valley_grade:
                matr_th[i, j] = 0
            else:
                coeff = ((sigma1 ** (-2)) + (sigma2 ** (-2)),
                         2 * (m2 / (sigma2 ** 2) - m1 / (sigma1 ** 2)),
                         2 * np.log((p2 * sigma1) / (p1 * sigma2)))
                sol = np.roots(coeff)
                matr_th[i, j] = (sol[0] if len(sol) == 1 or m1 < sol[0] < m2 else sol[1] if m1 < sol[0] < m2 else 0)

    return matr_th


def th_matrix_all(matr_th):
    matr_th_full = matr_th
    for i in range(matr_th.shape[0]):
        for j in range(matr_th.shape[1]):
            if matr_th[i, j] != 0:
                continue
            r = 1
            y_h = i - r  # high
            y_l = i + r  # low
            x_l = j - r  # left
            x_r = j + r  # right

            high_incl = True if y_h >= 0 else False
            low_incl = True if y_l < matr_th.shape[0] else False
            left_incl = True if x_l >= 0 else False
            right_incl = True if x_r < matr_th.shape[1] else False

            frame = np.array([])
            frame = (np.concatenate((frame,
                                     matr_th[y_h, (x_l + 1 if left_incl else 0):
                                                  (x_r if right_incl else matr_th.shape[1])]))
                     if high_incl else frame)  # include upper strip for frame without corners

            frame = (np.concatenate((frame,
                                     matr_th[y_l, (x_l + 1 if left_incl else 0):
                                                  (x_r if right_incl else matr_th.shape[1])]))
                     if low_incl else frame)  # include lower strip for frame without corners
            frame = (np.concatenate((frame,
                                     matr_th[(y_h + 1 if high_incl else 0):
                                             (y_l if low_incl else matr_th.shape[0]), x_l]))
                     if left_incl else frame)  # include left strip for frame without corners

            frame = (np.concatenate((frame,
                                     matr_th[(y_h + 1 if high_incl else 0):
                                             (y_l if low_incl else matr_th.shape[0]), x_r]))
                     if right_incl else frame)  # include right strip for frame without corners

            frame = np.append(frame, [
                (matr_th[y_h, x_l] if high_incl and left_incl else 0),  # left high corner
                (matr_th[y_h, x_r] if high_incl and right_incl else 0),  # right high corner
                (matr_th[y_l, x_l] if low_incl and left_incl else 0),  # left low corner
                (matr_th[y_l, x_r] if low_incl and right_incl else 0),  # right low corner
            ])

            while all(el == 0 for el in frame):
                r += 1

                y_h = i - r  # high
                y_l = i + r  # low
                x_l = j - r  # left
                x_r = j + r  # right

                frame = np.array([])
                frame = (np.concatenate((frame,
                                         matr_th[y_h, (x_l + 1 if left_incl else 0):
                                                      (x_r if right_incl else matr_th.shape[1])]))
                         if high_incl else frame)  # include upper strip for frame without corners

                frame = (np.concatenate((frame,
                                         matr_th[y_l, (x_l + 1 if left_incl else 0):
                                                      (x_r if right_incl else matr_th.shape[1])]))
                         if low_incl else frame)  # include lower strip for frame without corners
                frame = (np.concatenate((frame,
                                         matr_th[(y_h + 1 if high_incl else 0):
                                                 (y_l if low_incl else matr_th.shape[0]), x_l]))
                         if left_incl else frame)  # include left strip for frame without corners

                frame = (np.concatenate((frame,
                                         matr_th[(y_h + 1 if high_incl else 0):
                                                 (y_l if low_incl else matr_th.shape[0]), x_r]))
                         if right_incl else frame)  # include right strip for frame without corners

                frame = np.append(frame, [
                    (matr_th[y_h, x_l] if high_incl and left_incl else 0),  # left high corner
                    (matr_th[y_h, x_r] if high_incl and right_incl else 0),  # right high corner
                    (matr_th[y_l, x_l] if low_incl and left_incl else 0),  # left low corner
                    (matr_th[y_l, x_r] if low_incl and right_incl else 0),  # right low corner
                ])

            if frame.size != 0:
                matr_th_full[i, j] = sum(frame) / len([el for el in frame if el != 0])
            else:
                raise Exception
    return matr_th_full


def get_binar_chow_kaneko(matr_th, img, diam=7):
    bin_img = np.zeros_like(img, dtype=bool)
    for i in range(img.shape[0]):
        for j in range(img.shape[1]):
            i_matr = math.floor(i * 2 / diam)
            j_matr = math.floor(j * 2 / diam)
            a_incl, b_incl, c_incl, d_incl = (True,) * 4

            a = i - i_matr * diam / 2
            b = (i_matr + 1) * diam / 2 - i
            c = j - j_matr * diam / 2
            d = (j_matr + 1) * diam / 2 - j

            ta, tb, tc, td = (0,) * 4

            if i_matr == 0:
                a_incl, b_incl = False, False

            elif i_matr == matr_th.shape[0] - 1:
                c_incl, d_incl = False, False

            if j_matr == 0:
                a_incl, c_incl = False, False

            elif j_matr == matr_th.shape[1] - 1:
                b_incl, d_incl = False, False

            if a_incl:
                ta = matr_th[i_matr, j_matr]

            if b_incl:
                tb = matr_th[i_matr, j_matr + 1]

            if c_incl:
                tc = matr_th[i_matr + 1, j_matr]

            if d_incl:
                td = matr_th[i_matr + 1, j_matr + 1]

            t = (ta * b * d + tb * b * c + tc * a * d + td * a * c) / ((a + b) * (c + d))

            bin_img[i, j] = (False if t > img[i, j] else True)

    return bin_img


def binar_chow_kaneko(img, diam: int = 7, delta=4, valley_grade=0.8, eps: tuple = (0.05, 2)):

    img = get_semitone(img)

    matr_th = th_matrix_bimodal(img, diam, delta, valley_grade, eps)

    matr_th = th_matrix_all(matr_th)

    matr_th = smooth_matr(matr_th)

    bin_img = get_binar_chow_kaneko(matr_th, img, diam)

    return bin_img


def get_hist(img: np.ndarray):
    hist_br = np.zeros(256, dtype=np.uint8)
    for row in img:
        for pix in row:
            hist_br[pix] += 1
    return hist_br


def test_kaneko():
    d = 8
    img = get_semitone(open_image('book1.png'))
    # Image.fromarray(img).save('book_l.png')
    print('Получение бинарного изображения')
    start_time = time.time()
    img = binar_chow_kaneko(img, d, delta=8, eps=(0.05, 3), valley_grade=0.9)
    end_time = time.time()
    print('Показ бинарного изображения')
    print('Время бинаризации изображения:', round(end_time - start_time, 2), 'с')
    Image.fromarray(img).save('C:\\Users\\User\\Desktop\\ОАВИ\\7.png')

#1 - delta=10, eps=(0.1, 1.5)
#2 - delta=10
#3 - eps=(0.1, 1.5)
#4 - delta=20, eps=(0.05, 5)
#5 - delta=20, eps=(0.005, 100), valley_grade=0.3
#6 - delta=8, eps=(0.05, 2), valley_grade=0.7
#7 - delta=8, eps=(0.05, 3), valley_grade=0.9

def test_semitone():
    img = open_image(PATH)
    print('Показ исходного изображения...')
    Image.fromarray(img).show()
    print('Показ полутонового изображения...')
    img = get_semitone(img)
    Image.fromarray(img).show()


# Вариант 4
def test_christian():
    # Переменные бинаризации
    c = 0.5
    v = 8

    img = get_semitone(open_image(PATH))
    Image.fromarray(img).show()
    print('Получение бинарного изображения')
    start_time = time.time()
    img = christian(img, c, v)
    end_time = time.time()
    print('Показ бинарного изображения')
    print('Время бинаризации изображения:', round(end_time - start_time, 2), 'с')
    Image.fromarray(img).show()


if __name__ == '__main__':
    test_kaneko()
