# Лабораторная работа №3. 
## Фильтрация изображений и морфологические операции
В качестве входных данных берётся монохромное (или полутоновое, где уместно)
изображение (несколько штук). В качестве выходных данных демонстрируется:
1) отфильтрованное монохромное изображение
2) разностное изображение (попиксельный xor или модуль разности).
* _Для методов фильтрации полутоновых изображений дополнительно можно
попытаться применить результаты к цветному изображению, из которого было
получено начальное полутоновое изображение._ 

Вариант 10: Операция морфологического сжатия Erosion;

Исходное / отфильтрованное / разностное изображение книги в разных режимах:

1) 
<img height="50%" src="binar_book.png" width="50%"/>
<img height="50%" src="book_eros_B.png" width="50%"/>
<img height="50%" src="book_diff_B.png" width="50%"/>

2)
<img height="50%" src="book_L.png" width="50%"/>
<img height="50%" src="book_eros_L.png" width="50%"/>
<img height="50%" src="book_diff_L.png" width="50%"/>

3)

<img height="50%" src="book.png" width="50%"/>
<img height="50%" src="book_eros_RGB.png" width="50%"/>
<img height="50%" src="book_diff_RGB.png" width="50%"/>

Исходное / отфильтрованное / разностное изображение пейзажа в разных режимах:

1)
<img src="binar_landscape.png"/>
<img src="landscape_eros_B.png"/>
<img src="landscape_diff_B.png"/>

2)
<img src="landscape_L.png"/>
<img src="landscape_eros_L.png"/>
<img src="landscape_diff_L.png"/>

3)
<img src="landscape.png"/>
<img src="landscape_eros_RGB.png"/>
<img src="landscape_diff_RGB.png"/>