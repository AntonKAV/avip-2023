import numpy as np
from PIL import Image
import time
import matplotlib.pyplot as plt
import math

# PATH = 'binar_book.png'
PATH = 'binar_landscape.png'
# PATH = 'book.png'
# PATH = 'landscape.png'


def erosion(img, mode='L'):
    B = np.array([[0, 1, 1, 0],
                  [1, 1, 1, 1],
                  [1, 1, 1, 1],
                  [0, 1, 1, 0]])

    img = np.insert(img, 0, 1, 0)
    img = np.insert(img, img.shape[0], 1, 0)
    img = np.insert(img, img.shape[0], 1, 0)
    img = np.insert(img, 0, 1, 1)
    img = np.insert(img, img.shape[1], 1, 1)
    img = np.insert(img, img.shape[1], 1, 1)
    new_img = np.array(img)

    if mode == 'L':
        for i in range(1, img.shape[0] - 3):
            for j in range(1, img.shape[1] - 3):
                aper = img[i-1:i+3, j-1:j+3]
                minimum = 255
                for k in range(B.shape[0]):
                    for m in range(B.shape[1]):
                        if B[k, m] and minimum > aper[k, m]:
                            minimum = aper[k, m]
                new_img[i, j] = minimum
    elif mode == 'RGB':

        for i in range(1, img.shape[0] - 3):
            for j in range(1, img.shape[1] - 3):
                aper = img[i-1:i+3, j-1:j+3]
                minimum_r = 255
                minimum_g = 255
                minimum_b = 255
                for k in range(B.shape[0]):
                    for m in range(B.shape[1]):
                        if B[k, m]:
                            if minimum_r > aper[k, m][0]:
                                minimum_r = aper[k, m][0]
                            if minimum_g > aper[k, m][1]:
                                minimum_g = aper[k, m][1]
                            if minimum_b > aper[k, m][2]:
                                minimum_b = aper[k, m][2]
                new_img[i, j] = [minimum_r, minimum_g, minimum_b]

    return new_img[1:-2, 1:-2]


def pixel_xor(img1: np.ndarray, img2: np.ndarray, mode='L'):

    if img1.shape != img2.shape:
        raise ValueError
    new_img = np.zeros_like(img1)
    if mode == 'RGB':
        for i in range(new_img.shape[0]):
            for j in range(new_img.shape[1]):
                new_img[i, j] = [abs(img1[i, j][x] - img2[i, j][x]) for x in range(3)]
    else:
        for i in range(new_img.shape[0]):
            for j in range(new_img.shape[1]):
                new_img[i, j] = abs(img1[i, j] - img2[i, j])
    return new_img


def test_book():
    img = Image.open('book.png')
    img_sem = np.asarray(img.convert('L'))
    img = np.asarray(img)
    img_bin = np.asarray(Image.open('binar_book.png').convert('L'))

    Image.fromarray(img_bin).show()
    new_img = erosion(img_bin, mode='L')
    Image.fromarray(new_img).show()
    new_img = pixel_xor(img_bin, new_img, mode='L')
    Image.fromarray(new_img).show()

    Image.fromarray(img_sem).show()
    new_img = erosion(img_sem, mode='L')
    Image.fromarray(new_img).show()
    new_img = pixel_xor(img_sem, new_img, mode='L')
    Image.fromarray(new_img).show()

    Image.fromarray(img).show()
    new_img = erosion(img, mode='RGB')
    Image.fromarray(new_img).show()
    new_img = pixel_xor(img, new_img)
    Image.fromarray(new_img).show()

def test_landscape():
    img = Image.open('landscape.png')
    img_sem = np.asarray(img.convert('L'))
    img = np.asarray(img)
    img_bin = np.asarray(Image.open('binar_landscape.png').convert('L'))

    Image.fromarray(img_bin).show()
    new_img = erosion(img_bin, mode='L')
    Image.fromarray(new_img).show()
    new_img = pixel_xor(img_bin, new_img, mode='L')
    Image.fromarray(new_img).show()

    Image.fromarray(img_sem).show()
    new_img = erosion(img_sem, mode='L')
    Image.fromarray(new_img).show()
    new_img = pixel_xor(img_sem, new_img, mode='L')
    Image.fromarray(new_img).show()

    Image.fromarray(img).show()
    new_img = erosion(img, mode='RGB')
    Image.fromarray(new_img).show()
    new_img = pixel_xor(img, new_img)
    Image.fromarray(new_img).show()




if __name__ == '__main__':
    test_book()
