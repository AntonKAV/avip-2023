# Лабораторная работа №2
Задания:
1. Приведение полноцветного изображения к полутоновому. Новое изображение
создаётся в режиме полутона (1 яркостный канал, формат bmp), где яркость
каждого пикселя вычисляется (взвешенным) усреднением каналов исходного
полноцветного изображения.
2. Приведение полутонового изображения к монохромному с помощью алгоритма бинаризации Чоу и Канеко. 