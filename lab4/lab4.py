import math
import numpy as np
from PIL import Image
import time
import matplotlib.pyplot as plt

# PATH = 'pictures/landscape.png'
# PATH = '../pictures/xmpl.bmp'
# PATH = 'pictures/xmpl2.bmp'
# PATH = 'pictures/face_noise.jpg'
# PATH = '../pictures/book.png'
PATH = '../pictures/scarface.png'
# PATH = '../pictures/300px-Bikesgray.png'
mode = 'RGB'


def summ(a, b):
    return abs(a) + abs(b)


def prewitt(img: np.ndarray):
    gx = np.array([[-1, -1, -1, -1, -1],
                   [0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0],
                   [0, 0, 0, 0, 0],
                   [1, 1, 1, 1, 1]])
    gy = np.array([[-1, 0, 0, 0, 1],
                   [-1, 0, 0, 0, 1],
                   [-1, 0, 0, 0, 1],
                   [-1, 0, 0, 0, 1],
                   [-1, 0, 0, 0, 1]])
    # gx = np.array([[-1, -1, -1],
    #               [0, 0, 0],
    #               [1, 1, 1]])
    # gy = np.array([[-1, 0, 1],
    #                [-1, 0, 1],
    #                [-1, 0, 1]])

    img = np.insert(img, [0, img.shape[1]]*(gx.shape[0]//2), 0, axis=1)
    img = np.insert(img, [0, img.shape[0]]*(gx.shape[1]//2), 0, axis=0)

    img_x = np.zeros_like(img[(gx.shape[0]//2):-(gx.shape[0]//2), (gx.shape[1]//2):-(gx.shape[1]//2)])
    img_y = np.zeros_like(img[(gx.shape[0]//2):-(gx.shape[0]//2), (gx.shape[1]//2):-(gx.shape[1]//2)])
    new_img = np.zeros_like(img[(gx.shape[0]//2):-(gx.shape[0]//2), (gx.shape[1]//2):-(gx.shape[1]//2)])

    for i in range(img_x.shape[0]):
        for j in range(img_x.shape[1]):
            img_x[i, j] = abs(np.sum(gx*img[i:i+gx.shape[0], j:j+gx.shape[1]]))

    M = np.max(img_x)
    for i in range(img_x.shape[0]):
        for j in range(img_x.shape[1]):
            img_x[i, j] = 255 * img_x[i, j] / M


    for i in range(img_y.shape[0]):
        for j in range(img_y.shape[1]):
            img_y[i, j] = abs(np.sum(gy*img[i:i+gy.shape[0], j:j+gy.shape[1]]))

    M = np.max(img_y)
    for i in range(img_y.shape[0]):
        for j in range(img_y.shape[1]):
            img_y[i, j] = 255 * img_y[i, j] / M


    for i in range(new_img.shape[0]):
        for j in range(new_img.shape[1]):
            new_img[i, j] = summ(img_x[i, j], img_y[i, j])

    M = np.max(new_img)
    for i in range(new_img.shape[0]):
        for j in range(new_img.shape[1]):
            new_img[i, j] = 255 * new_img[i, j] / M

    return img_x, img_y, new_img


def get_hist(img: np.ndarray):
    hist_br = np.zeros(256, dtype=np.uint8)
    for row in img:
        for pix in row:
            hist_br[pix] += 1
    return hist_br


def smooth(hist: np.ndarray(shape=[256], dtype=np.uint8)):
    new_hist = np.zeros_like(hist)
    for i in range(hist.shape[0]):
        new_hist[i] = ((hist[i - 2] if i > 1 else 0) +
                       (2 * hist[i - 1] if i > 0 else 0) +
                       3 * hist[i] +
                       (2 * hist[i + 1] if i < hist.shape[0] - 1 else 0) +
                       (hist[i + 2] if i < hist.shape[0] - 2 else 0)) / 9
    return new_hist


if __name__ == '__main__':
    img = np.asarray(Image.open('book_grad.png').convert('L'))
    binar = np.array([[False if pix < 150 else True for pix in row] for row in img])
    Image.fromarray(binar).save('book_binar.png')
