import random

n, m, q = [int(x) for x in input().split()]
r = [0]*n
a = [[1]*m]*n
a_sum = [m]*n
r_a_sum_dot = [0]*n
result = []
max_val = 0
max_index = 0
min_val = 0
min_index = 0
is_changed_max = False
is_changed_min = False
for i in range(q):
    line = [x for x in input().split()]
    match line[0]:
        case 'DISABLE':
            ind_1 = int(line[1]) - 1
            ind_2 = int(line[2]) - 1
            if a[ind_1][ind_2] == 1:
                a_sum[ind_1] -= 1
                a[ind_1][ind_2] = 0
                cur_val = r[ind_1] * a_sum[ind_1]
                r_a_sum_dot[ind_1] = cur_val
                if ind_1 == max_index:
                    is_changed_max = True
                if ind_1 == min_index:
                    is_changed_min = True
                if cur_val > max_val:
                    max_val = cur_val
                    max_index = ind_1
                    is_changed_max = False
                if cur_val < min_val:
                    min_val = cur_val
                    min_index = ind_1
                    is_changed_min = False
        case 'GETMAX':
            if is_changed_max:
                max_val = max(r_a_sum_dot)
                max_index = r_a_sum_dot.index(max_val)
                is_changed_max = False

            # result += [max_index + 1]
            print(max_index + 1)
        case 'GETMIN':
            if is_changed_min:
                min_val = min(r_a_sum_dot)
                min_index = r_a_sum_dot.index(min_val)
                is_changed_min = False

            # result += [min_index + 1]
            print(min_index + 1)
        case 'RESET':
            ind_1 = int(line[1]) - 1
            r[ind_1] += 1
            a_sum[ind_1] = m
            a[ind_1] = [1]*m
            cur_val = r[ind_1] * a_sum[ind_1]
            r_a_sum_dot[ind_1] = cur_val
            if ind_1 == max_index:
                is_changed_max = True
            if ind_1 == min_index:
                is_changed_min = True
            if cur_val > max_val:
                max_val = cur_val
                max_index = ind_1
                is_changed_max = False
            if cur_val < min_val:
                min_val = cur_val
                min_index = ind_1
                is_changed_min = False


[print(x) for x in result]