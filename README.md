#  Обработка аудиовизуальной информации<br>(Audiovisual Information Processing)
НИЯУ МИФИ<br>Весенний семестр 2023 года<br><br>_В качестве основных инструментов использовались:_
1. _Python (3.9.13)_
2. _Numpy (1.22.3)_
3. _Pillow (9.1.0)_
4. _Matplotlib (3.6.2)_

Для установки всех необходимых модулей в python воспользоваться командой:
`pip install -r requirements.txt`

***
## Лабораторная работа №1
Необходимо разработать методы для работы с изображениями, позволяющие выполнять следующий функционал:
1) Растяжение (интерполяция) изображения в M раз;
2) Сжатие (децимация) изображения в N раз;
3) Передискретизация изображения в K=M/N раз путём растяжения и
последующего сжатия (в два прохода);
4) Передискретизация изображения в K раз за один проход.

Демонстрируется результат каждой операции (до и после).