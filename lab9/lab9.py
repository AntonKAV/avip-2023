import numpy as np
import scipy.signal as signal
import matplotlib.pyplot as plt
import librosa
from scipy.io import wavfile
import soundfile as sf


def get_spectrogram(filename):
    audio, sr = librosa.load(filename, sr=None)

    window_size = 1024  # Size of the window for the STFT
    hop_length = int(window_size / 4)  # Hop size between consecutive windows
    n_fft = window_size


    spectrogram = librosa.stft(audio, n_fft=n_fft, hop_length=hop_length, window=signal.hann(window_size))
    spectrogram_db = librosa.amplitude_to_db(np.abs(spectrogram), ref=np.max)


    # Plot the spectrogram
    plt.figure(figsize=(12, 5))
    librosa.display.specshow(spectrogram_db, sr=sr, hop_length=hop_length, x_axis='time', y_axis='log')
    plt.colorbar(format='%+2.0f dB')
    plt.title('Spectrogram')
    plt.xlabel('Time')
    plt.ylabel('Frequency')
    return plt


if __name__ == '__main__':
    sample_rate, data = wavfile.read('drum.wav')
    data = np.array([x[0] for x in data])
    data = data.astype(float)

    window_length = 51
    polyorder = 3
    filtered_data = signal.savgol_filter(data, window_length, polyorder)

    filtered_data = filtered_data.astype('int16')
    wavfile.write('drum_filtered_via_savgol_filter.wav', sample_rate, filtered_data)

    spectrogram_original = get_spectrogram("drum.wav")
    spectrogram_original.savefig('spectrogram_original.png')

    spectrogram_savgol_filter = get_spectrogram("drum_filtered_via_savgol_filter.wav")
    spectrogram_savgol_filter.savefig('spectrogram_savgol_filter.png')

    sample_rate, data = wavfile.read("drum.wav")
    data = data.astype(float)

    filtered_data = signal.wiener(data)

    filtered_data = filtered_data.astype('int16')
    wavfile.write('drum_filtered_via_wiener.wav', sample_rate, filtered_data)

    spectrogram_wiener = get_spectrogram("drum_filtered_via_wiener.wav")
    spectrogram_wiener.savefig("spectrogram_wiener.png")

    audio, sr = librosa.load('drum.wav', sr=None)
    RMS = np.sqrt(np.mean(audio ** 2))
    STD_n = 0.001
    noise = np.random.normal(0, STD_n, audio.shape[0])
    audio_noise = audio + noise
    sf.write('drum_with_noise.wav', audio_noise, sr)

    # Savgol filter for noise audiofile
    sample_rate, data = wavfile.read('drum_with_noise.wav')
    data = data.astype(float)
    window_length = 51
    polyorder = 3

    filtered_data = signal.savgol_filter(data, window_length, polyorder)
    filtered_data = filtered_data.astype('int16')

    wavfile.write('drum_with_noise_via_savgol_filter.wav', sample_rate, filtered_data)

    spectrogram_noise = get_spectrogram("drum_with_noise.wav")
    spectrogram_noise.savefig("spectrogram_noise.png")

    spectrogram_noise_via_savgol = get_spectrogram("drum_with_noise_via_savgol_filter.wav")
    spectrogram_noise_via_savgol.savefig("spectrogram_noise_via_savgol.png")
